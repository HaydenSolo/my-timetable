# Copyright (C) 2017  Jamie McClymont, Rhys Davies, and Nick Webster
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from mytimetable.util import iso_to_date
from collections import namedtuple

from icalendar import Calendar, Event, vRecur, vFrequency
import datetime as dt
import pytz

localtz = pytz.timezone('Pacific/Auckland')

days = {'1': 'MO', '2': 'TU', '3': 'WE', '4': 'TH', '5': 'FR', '6': 'SA', '7': 'SU'}


def find_instances(instance_set, key_dates, trimester_n):
    trimester_range = key_dates.trimester_ranges[trimester_n]

    start = localtz.localize(dt.datetime.combine(iso_to_date(trimester_range[0].year, instance_set[0]
                                                             .weeks[0][0], instance_set[0].dow),
                                                 instance_set[0].time_range[0]))

    end = localtz.localize(dt.datetime.combine(iso_to_date(trimester_range[0].year, instance_set[0]
                                                           .weeks[0][0], instance_set[0].dow),
                                               instance_set[0].time_range[1]))

    until = localtz.localize(dt.datetime.combine(iso_to_date(trimester_range[0].year, instance_set[-1]
                                                             .weeks[-1][1], instance_set[-1].dow),
                                                 instance_set[-1].time_range[0]))
    days_of_week = []
    for inst in instance_set:
        days_of_week.append(days[localtz.localize(dt.datetime.combine(iso_to_date(trimester_range[0].year,
                                                                                  inst.weeks[0][0], inst.dow),
                                                                      inst.time_range[0])).strftime("%u")])

    exceptions = []
    for inst in instance_set:
        for i in range(1, len(inst.weeks)):
            end_week = inst.weeks[i - 1][1]
            start_again = inst.weeks[i][0]
            for j in range(end_week + 1, start_again):
                exceptions.append(localtz.localize(dt.datetime.combine(iso_to_date(trimester_range[0].year,
                                                                                   j, inst.dow),
                                                                       inst.time_range[0])))
    for inst in instance_set:
        for weeks in inst.weeks:
            start_week = iso_to_date(trimester_range[0].year, weeks[0], 1)
            end_week = iso_to_date(trimester_range[0].year, weeks[1], 7)
            for day in key_dates.closed_dates:
                if (start_week < day < end_week) and (day.weekday() + 1 == inst.dow):
                    exceptions.append(localtz.localize(dt.datetime.combine(day, inst.time_range[0])))

    Instance = namedtuple('Instance', 'start end until days_of_week exceptions')

    return Instance(start, end, until, days_of_week, exceptions)


def make_calendar(trimester, key_dates, trimester_n):
    cal = Calendar()

    cal.add('prodid', "MyTimetable for Victoria University by Jamie McClymont <jamie.mcclymont@gmail.com>, "
                      "Rhys Davies <rhys@johnguant.com>, and Nick Webster <nick@nick.geek.nz>")
    cal.add('version', '2.0')

    for activity, instance_sets in trimester.activity_instance_sets.items():
        event = Event()

        instances = find_instances(instance_sets, key_dates, trimester_n)

        event.add('uid', "{},{},{}@my-timetable".format(activity.name, instances.start, dt.datetime.today().year))
        event.add('dtstamp', dt.datetime.utcnow())
        event.add('created', dt.datetime.utcnow())
        event.add('summary', activity.name)
        event.add('location', "{} ({})".format(activity.location,
                                               activity.zone))

        cal.add_component(event)

        event.add('dtstart', instances.start)
        event.add('dtend',   instances.end)

        repeat = vRecur()
        repeat['FREQ'] = vFrequency("WEEKLY")
        repeat['UNTIL'] = instances.until
        repeat['BYDAY'] = instances.days_of_week

        event.add('rrule', repeat)

        event.add('exdate', instances.exceptions)

    return cal.to_ical()
