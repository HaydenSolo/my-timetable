def determine_current_trimester(key_dates, today):
    if len(key_dates.trimester_ranges[2]) == 0 or today > key_dates.trimester_ranges[2][1]:
        return 3
    if len(key_dates.trimester_ranges[1]) == 0 or today > key_dates.trimester_ranges[1][1]:
        return 2

    return 1
