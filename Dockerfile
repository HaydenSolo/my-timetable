FROM alpine

EXPOSE 8000

RUN apk add --no-cache build-base libffi-dev openssl-dev libxml2-dev libxslt-dev uwsgi-python py-pip python3-dev

RUN python3 -m pip install poetry

WORKDIR /app

ADD . /app

RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

ENTRYPOINT exec /usr/sbin/uwsgi --socket "0.0.0.0:8000" \
               "--uid" "uwsgi" \
               "--plugins" "python" \
               "--protocol" "uwsgi" \
               "--file" "server.py" \
               "--callable" "server"
